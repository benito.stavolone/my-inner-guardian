﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class AnimationRotation : MonoBehaviour
{

    [SerializeField] private string ChooseAxis;

    public float speed;

    private bool isVisible = false;

    private void Update()
    {
        if (isVisible)
        {
            switch (ChooseAxis)
            {
                case "x":
                    transform.Rotate(2, 0, 0 * Time.deltaTime * speed);
                    break;
                case "y":
                    transform.Rotate(0, 2, 0 * Time.deltaTime * speed);
                    break;
                case "z":
                    transform.Rotate(0, 0, 2 * Time.deltaTime * speed);
                    break;
            }
        }
    }

    private void Rotation()
    {
        transform.Rotate(0, 2, 0 * Time.deltaTime * speed);
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }
}
