﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumAnimationController : MonoBehaviour
{

    [SerializeField] private float maxAngleDeflection = 30f;
    [SerializeField] private float speedOfPendulum = 1f;

    [Header("Information About Axis")]
    
    [SerializeField] private float setCurrentAxis;
    [SerializeField] private string ChooseAxis;

    private bool isVisible = false;

    // Update is called once per frame
    void Update()
    {
        if (isVisible)
        {
            float angle = maxAngleDeflection * Mathf.Sin(Time.time * speedOfPendulum);

            switch (ChooseAxis)
            {
                case "x":
                    transform.localRotation = Quaternion.Euler(angle + setCurrentAxis, transform.rotation.y, transform.rotation.z);
                    break;
                case "y":
                    transform.localRotation = Quaternion.Euler(transform.rotation.x, angle + setCurrentAxis, transform.rotation.z);
                    break;
                case "z":
                    transform.localRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, angle + setCurrentAxis);
                    break;
            }
        }      
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }
}
