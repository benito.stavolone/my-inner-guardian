﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PanelSlider : MonoBehaviour
{
    [SerializeField] private Animator _panelAnimation;

    private void Awake()
    {
        _panelAnimation = GetComponent<Animator>();
    }
    private void OnEnable()
    {
        _panelAnimation.SetBool("AnimationPanel",true);
    }

    private void OnDisable()
    {
        _panelAnimation.SetBool("AnimationPanel",false);
    }
}
