﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FilledLifeBar : MonoBehaviour
{
    [SerializeField] Image FillLife;

    public void FillBar(float filled)
    {
        FillLife.fillAmount = filled;
    }
}
