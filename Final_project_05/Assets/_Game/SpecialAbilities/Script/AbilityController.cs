﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityController : MonoBehaviour
{
    [SerializeField] private List<MonoBehaviour> classList;
    [SerializeField] private List<GameObject> availablePower;
    [SerializeField] private LayerMask collisionMask;
    [SerializeField] private float msBetweenShots = 100;
    [SerializeField] private float powerSpeed;
    [SerializeField] private Transform powerHold;
    [Header("Power Sprite")]
    [SerializeField] private List<Image> powerSprite;

    private int[] projectileperPower;
    private GameObject equipedPower;
    private int selectedPower = 0;
    private float nextShotTime;
    private GameManager gameManagerRef;


    void Start()
    {
        gameManagerRef = GameManager.instance;
        SelectPower();
        InitializeProjectileList();     
    }

    void Update()
    {
        if (gameManagerRef.gameState == GameState.PAUSE)
            return;

        int previousSelectedPower = selectedPower;

        if (!GameManager.instance.useController)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (selectedPower >= availablePower.Count - 1)
                    selectedPower = 0;
                else
                    selectedPower++;
            }
        }

        if (GameManager.instance.useController)
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button4))
            {
                if (selectedPower >= availablePower.Count - 1)
                    selectedPower = 0;
                else
                    selectedPower++;
            }
        }


        if (previousSelectedPower != selectedPower)
        {
            SelectPower();
        }

        GamePlayGUIManager.instance.RefreshAmmoOfPower(projectileperPower[selectedPower]);
    }

    private void SelectPower()
    {
        int i = 0;
        foreach (GameObject power in availablePower)
        {
            if (i == selectedPower)
            {
                power.gameObject.SetActive(true);
                equipedPower = availablePower[selectedPower];
            }
            else
            {
                power.gameObject.SetActive(false);
            }
            i++;
        }

        SetUiSprite();
        GamePlayGUIManager.instance.RefreshCurrentTypeOfPower(powerSprite[selectedPower]);
    }

    private void SetUiSprite()
    {
        int index = 0;

        for(int i = 0; i < powerSprite.Count; i++)
        {
            if (powerSprite[i] != powerSprite[selectedPower])
            {
                GamePlayGUIManager.instance.RefreshTypeOfPower1(powerSprite[i], index);
                index++;
            }
        }
    }

    private void Shoot()
    {
        if (Time.time > nextShotTime && projectileperPower[selectedPower] > 0)
        {
            projectileperPower[selectedPower]--;
            nextShotTime = Time.time + msBetweenShots / 1000;
            GameObject newPower = Instantiate(equipedPower, powerHold.position, powerHold.rotation);
           
        }
    }

    public void OnTriggerHold()
    {
        Shoot();
    }

    private void InitializeProjectileList()
    {
        projectileperPower = new int[availablePower.Count];
        for (int i = 0; i < projectileperPower.Length; i++)
            projectileperPower[i] = 2;
    }

    public void SetMoreProjectile(int number, int index)
    {
        projectileperPower[index] += number;
    }

    public void AimPoint(Vector3 aimPoint)
    {
        powerHold.LookAt(aimPoint);
    }
}
