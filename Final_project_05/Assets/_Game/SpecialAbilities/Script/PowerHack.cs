﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerHack : Power
{
    private AudioSource _audioSource;
    //[SerializeField] private AudioClip clip;
    //private void Awake()
    //{
    //    _audioSource = GetComponent<AudioSource>();
    //}
    public override void OnHitObject(Collider collision, Vector3 hitPoint)
    {
        IMinigable minigable = collision.GetComponent<IMinigable>();
        if (minigable != null)
        {
            minigable.Hacking();
        }
        GameObject.Destroy(gameObject);
    }

}
