﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerFire : Power
{
    [SerializeField] private AudioClip clip;
    [SerializeField] private GameObject burningParticle;

    public override void OnHitObject(Collider collision, Vector3 hitPoint)
    {
        IBurnable burnable = collision.GetComponent<IBurnable>();
        if (burnable != null)
        {
            GameObject fire = Instantiate(burningParticle, collision.gameObject.transform.position, Quaternion.identity);
            fire.transform.SetParent(collision.gameObject.transform);
            collision.gameObject.GetComponent<Enemy>().FireReference(fire);
            burnable.Burning();          
        }
        GameObject.Destroy(gameObject);
    }
}
