﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using TMPro;

public class PowerMind : Power
{
    private AudioSource _audioSource;
    [SerializeField] private GameObject mindControlParticle;
    [SerializeField] private GameObject floatingText;

    public override void OnHitObject(Collider collision, Vector3 hitPoint)
    {
        IBrainWashed brainwash = collision.GetComponent<IBrainWashed>();
        if (brainwash != null)
        {
            GameObject mindControl = Instantiate(mindControlParticle, collision.gameObject.transform.position, Quaternion.identity);
            mindControl.transform.SetParent(collision.gameObject.transform);
            collision.gameObject.GetComponent<Enemy>().MindControlReference(mindControl);

            brainwash.ChangeTarget();
        }
        else
        {
            if(collision.GetComponent<Enemy>() != null)
            {
                GameObject text = Instantiate(floatingText, new Vector3(collision.gameObject.transform.position.x , collision.gameObject.transform.position.y + 2, collision.gameObject.transform.position.z - 2), Quaternion.identity);
                var textmesh = text.GetComponent<TextMeshPro>();

                textmesh.text = "No Effect";
            }


        }
        GameObject.Destroy(gameObject);
    }

}
