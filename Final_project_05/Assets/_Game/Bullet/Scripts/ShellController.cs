﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Security.Cryptography;
using UnityEngine;

public class ShellController : MonoBehaviour
{

    public Rigidbody myRigidbody;
    public float forceMin;
    public float forceMax;

    private float lifetime = 0.7f;

    // Start is called before the first frame update
    void OnEnable()
    {
        myRigidbody.AddForce(transform.right * Random.Range(forceMin, forceMax));
        Invoke("Reset", lifetime);
    }

    private void Reset()
    {
        ObjectPool.instance.PutInPool("Shell", gameObject);
        myRigidbody.velocity = Vector3.zero;
    }
}
