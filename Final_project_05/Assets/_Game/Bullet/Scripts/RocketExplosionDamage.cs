﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketExplosionDamage : MonoBehaviour
{
    public float radius = 10;
    public Projectile projectileRef;
    private float explosionDamage;

    private LayerMask layerWall;

    private void Start()
    {
        explosionDamage = projectileRef.damage;
    }

    private void OnDestroy()
    {
        SpawnExplosionEffect();
        Vector3 origin = transform.position;
        layerWall = LayerMask.GetMask("Wall");
        RaycastHit rh;

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach (Collider hit in colliders)
        {
            IDamageable damageableObject = hit.GetComponent<IDamageable>();
            if (damageableObject != null)
            {
                if(Physics.Raycast(transform.position, hit.transform.position-transform.position, out rh, Mathf.Infinity))
                {
                    if (rh.collider.gameObject.layer != Mathf.Log(layerWall, 2))
                    {
                        damageableObject.TakeDamage(Mathf.Abs((explosionDamage - Mathf.Abs(Vector3.Distance(transform.position,hit.transform.position)))/2 -2));
                    }
                }                                       
            }
        }
    }

    private void SpawnExplosionEffect()
    {
        ObjectPool.instance.SpawnFromPool("Explosion_PS", transform.position, Quaternion.identity);
    }
}
