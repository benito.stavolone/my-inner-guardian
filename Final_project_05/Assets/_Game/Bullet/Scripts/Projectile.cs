﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private LayerMask collisionMask;
    private LayerMask destructionMask;
    private float speed = 15;

    public float damage = 1;

    private float lifetime = 5;

    private void Start()
    {
        if(collisionMask != 8)
        {
            Collider[] initialCollision = Physics.OverlapSphere(transform.position, 0.01f, collisionMask);
            if (initialCollision.Length > 0)
            {
                OnHitObject(initialCollision[0], transform.position);
            }
        }
        destructionMask = LayerMask.GetMask("Wall");
        Destroy(gameObject, lifetime);
    }

    // Update is called once per frame
    void Update()
    {
            float moveDistance = speed * Time.deltaTime;
            CheckCollision(moveDistance);
            transform.Translate(Vector3.forward * moveDistance);
    }

    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

    public void SetCollisionMask(LayerMask newLayer)
    {
        collisionMask = newLayer;
    }

    private void CheckCollision(float moveDistance)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, moveDistance, collisionMask, QueryTriggerInteraction.Collide))
        {
            OnHitObject(hit.collider,hit.point);
            SpawnParticleEffect(hit.point);

        }
        else if (Physics.Raycast(ray, out hit, moveDistance, destructionMask, QueryTriggerInteraction.Collide))
        {
            Destroy(gameObject);
            SpawnParticleEffect(hit.point);
        }
    }

    private void OnHitObject(Collider collision, Vector3 hitPoint)
    {
        IDamageable damageableObject = collision.GetComponent<IDamageable>();
        if (damageableObject != null)
        {
            damageableObject.TakeHit(damage, hitPoint, transform.forward);
        }
        GameObject.Destroy(gameObject);
    }

    private void SpawnParticleEffect(Vector3 hitPoint)
    {
        ObjectPool.instance.SpawnFromPool("Hit_PS", hitPoint, Quaternion.identity);
    }
}
