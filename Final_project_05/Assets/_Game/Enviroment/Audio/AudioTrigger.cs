﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour
{
    [SerializeField] AudioClip SoundPlay;
    [SerializeField] float Volume;
    [SerializeField] private bool alreadyPlayed = false;
    AudioSource audio;
    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter()
    {
        if(!alreadyPlayed)
        {
            audio.PlayOneShot(SoundPlay, Volume);
            alreadyPlayed = true;
        }
    }
}
