﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivePanel : MonoBehaviour
{
    [SerializeField] private GameObject _roof;
    private void OnTriggerEnter(Collider other)
    {
        _roof.SetActive(false);
    }
}
