﻿using AuraAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashingLiht : MonoBehaviour
{

    [Range(0, 3)] 
    [SerializeField] public float totalSeconds;

    private Light myLight;
    private AuraLight myAuraLight;
    private float currentTime = 0;
    private bool isVisible = false;

    void Start()
    {
        myLight = GetComponent<Light>();

        if (GetComponent<AuraLight>() != null)
            myAuraLight = GetComponent<AuraLight>();
        else
            myAuraLight = null;
    }

    void Update()
    {
        Flash();
    }

    private void Flash()
    {
        float waitTime = Random.Range(totalSeconds / 2, totalSeconds);

        if(Time.time - currentTime < waitTime)
        {
            myLight.enabled = true;
            if (myAuraLight != null)
                myAuraLight.enabled = true;
        }
        else
        {
            myLight.enabled = false;
            if (myAuraLight != null)
                myAuraLight.enabled = true;
            currentTime = Time.time;
        }
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }

    private void OnBecameInvisible()
    {
        isVisible = false;
    }
}
