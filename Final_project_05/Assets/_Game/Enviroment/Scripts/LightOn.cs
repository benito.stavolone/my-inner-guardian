﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LightOn : MonoBehaviour
{
    [SerializeField] private List<Light> _light;
    private bool _enterthetrigger = false;

    private void Awake()
    {
        foreach(Light l in _light)
        {
            l.gameObject.SetActive(true);
            l.intensity = 0;
        }

        //_light.ForEach(l => l.gameObject.SetActive(true)); //forma abbreviata del foreach
        //lamda()=> {}
    }
    private void OnTriggerEnter(Collider other)
    {
        if(!_enterthetrigger)
        {
            _enterthetrigger = true;
            TurnOnLight();
        }
    }

    public void TurnOnLight()
    {
        StartCoroutine(IntensityUpCorutine());
    }

    IEnumerator IntensityUpCorutine()
    {
        float intensity = 0;
        while(intensity < 1)
        {
            _light.ForEach( l => l.intensity = intensity);
            intensity += Time.deltaTime;
            yield return null;
        }
    }
}
