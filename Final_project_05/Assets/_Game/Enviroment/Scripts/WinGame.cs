﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinGame : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GameManager.instance.gameState = GameState.WIN;
        SceneManager.LoadScene(3);
    }
}
