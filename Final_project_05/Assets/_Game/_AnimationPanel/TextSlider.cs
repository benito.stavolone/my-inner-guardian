﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextSlider : MonoBehaviour
{
    [SerializeField] private Animator _textAnimator;

    private void Awake()
    {
        _textAnimator = GetComponent<Animator>();
    }
    private void OnEnable()
    {
        _textAnimator.SetBool("TextAnimation",true);
    }

    private void OnDisable()
    {
        _textAnimator.SetBool("TextAnimation",false);
    }
}
