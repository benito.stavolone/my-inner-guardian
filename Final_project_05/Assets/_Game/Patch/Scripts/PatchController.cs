﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PatchController : MonoBehaviour
{
    [SerializeField] private int bonusAmmoInMagazine=2;
    [SerializeField] private float bonusReloadTime = 0.1f;
    [SerializeField] private float bonusHealth = 5;
    [SerializeField] private int bonusNumberofPowerHit = 3;
    [SerializeField] private GameObject floatingText;

    [Header("PercentageToSpawn")]
    public Spawn[] upgradeSpawn = new Spawn[6];
    private int maxHealtUpgrade = 150;
    private GameObject playerRef;
    private int typeOfUpgrade;

    [Header("Text")]
    [SerializeField] private List<string> textName = new List<string>(6);

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            playerRef = other.gameObject;
            UpgradeController();
            Destroy(gameObject);
        }
    }

    private void UpgradeController()
    {
        int i = Random.Range(0, 101);

        for (int j=0; j < upgradeSpawn.Length; j++)
        {
            if (i >= upgradeSpawn[j].minProbabilityRange && i <= upgradeSpawn[j].maximunProbabilityRange)
            {
                typeOfUpgrade = j;
                break;
            }
        }

        GameObject text = Instantiate(floatingText, transform.position, Quaternion.identity);
        var textmesh = text.GetComponent<TextMeshPro>();

        switch (typeOfUpgrade)
        {
            case 0:             
                playerRef.GetComponent<GunController>().SetMoreAmmoinMagazine(bonusAmmoInMagazine);
                break;
            case 1:              
                playerRef.GetComponent<GunController>().SetNewReloadTime(bonusReloadTime);
                break;
            case 2:                
                if (playerRef.GetComponent<LivingEntity>().startinHealth <= (maxHealtUpgrade - bonusHealth))
                {
                    playerRef.GetComponent<LivingEntity>().startinHealth += bonusHealth;
                }                  
                break;
            case 3:              
                playerRef.GetComponent<AbilityController>().SetMoreProjectile(bonusNumberofPowerHit, 0);
                break;
            case 4:                
                playerRef.GetComponent<AbilityController>().SetMoreProjectile(bonusNumberofPowerHit, 1);
                break;
            case 5:
                playerRef.GetComponent<AbilityController>().SetMoreProjectile(bonusNumberofPowerHit, 2);
                break;
        }

        textmesh.text = textName[typeOfUpgrade];
    }
}

[System.Serializable]
public class Spawn
{
    public string TypeofUpgrade;
    public int minProbabilityRange = 0;
    public int maximunProbabilityRange = 0;
}