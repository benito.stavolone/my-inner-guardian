﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingTextController : MonoBehaviour
{

    [SerializeField] private float disappearTime = 2;
    [SerializeField] private float speed = 4;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, disappearTime);
    }

    private void Update()
    {
        transform.position += Vector3.up * Time.fixedDeltaTime * speed;
    }

}
