﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyShooterController : Enemy, IBurnable, IMinigable ,IBrainWashed
{
    public float speed = 10;
    [SerializeField] private GameObject lifeBar;
    public float stoppingDistance=20;
    public float retretDistance = 15;
    public float muzzleVelocity;
    public float startTimeBtwShots;

    public float distanceFromPlayer = 40;

    [Header("BurnEffect")]
    public float burnDamage = 1;
    public float burnTime = 5;
    private float currentBurnTime;

    [Header("BrainwashEffect")]
    public float brainwashTime = 5;
    public LayerMask brainwashCollision;

    public Projectile projectile;
    public GameObject projectileEffect;
    public Transform[] firePoint;

    public LayerMask layer;
    private RaycastHit hit;
    private LivingEntity targetEntity;

    private Transform target;
    private float timeBetweenShots;
    private bool hasTarget;
    private bool isMinigameTime = false;
    private bool isVisible = false;
    private LivingEntity healthEntity;
    private bool isBrainwashed = false;

    private MiniGameController _miniGameController;

    private void Awake()
    {
        _miniGameController = FindObjectOfType<MiniGameController>();
    }

   
    protected override void Start()
    {
        base.Start();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        targetEntity = target.GetComponent<LivingEntity>();
        targetEntity.OnDeath += OnTargetDeath;
        hasTarget = true;
        timeBetweenShots = startTimeBtwShots;
        healthEntity = transform.GetComponent<LivingEntity>();
        currentBurnTime = burnTime;
    }


    void Update()
    {
        if (GameManager.instance.Isminigamerunning)
        {
            return;
        }
        else
        {
            if (isVisible)
            {
                if (hasTarget && Vector3.Distance(transform.position, target.position) < distanceFromPlayer && !isMinigameTime)
                {
                    transform.LookAt(target);
                    StartCoroutine(SearchPlayer());
                }

                if (!hasTarget && isBrainwashed)
                {
                    FindClosestEnemy();
                }

            }
        }  
    }

    IEnumerator SearchPlayer()
    {
        int refreshRate = 2;

        if (Physics.Raycast(transform.position, transform.forward, out hit, distanceFromPlayer))
        {
            if (hit.collider.gameObject.layer == Mathf.Log(layer.value, 2))
            {
                lifeBar.SetActive(true);
                StartCoroutine(UpdateDistance());
                Shoot();
            }
        }
        yield return new WaitForSeconds(refreshRate);
    }         

    IEnumerator UpdateDistance()
    {
        float refreshRate = 1f;

        if(Vector3.Distance(transform.position, target.position) > stoppingDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
        else if (Vector3.Distance(transform.position, target.position) < stoppingDistance &&
            Vector3.Distance(transform.position, target.position) > retretDistance)
        {
            transform.position = this.transform.position;
        }
        else if(Vector3.Distance(transform.position, target.position) < retretDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, -speed * Time.deltaTime);
        }

        yield return refreshRate;
    }

    private void Shoot()
    {
        if (timeBetweenShots <= 0)
        {
            foreach(Transform point in firePoint)
            {
                Projectile newBullet = Instantiate(projectile, point.transform.position, point.transform.rotation) as Projectile;
                newBullet.SetCollisionMask(layer);
                newBullet.SetSpeed(muzzleVelocity);
            }
            timeBetweenShots = startTimeBtwShots;
        }
        else
        {
            timeBetweenShots -= Time.deltaTime;
        }
    }

    private void OnTargetDeath()
    {
        hasTarget = false;
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }

    public void Burning()
    {
        if (currentBurnTime > 0)
        {
            healthEntity.TakeDamage(burnDamage);
            currentBurnTime--;
            Invoke("Burning", 1);
        }
        else
            currentBurnTime = burnTime;
    }

    public void Hacking()
    {
        _miniGameController.StartRandomMinigame(this.gameObject);
    }

    public void ChangeTarget()
    {
        isBrainwashed = true;
        transform.GetComponent<MeshRenderer>().material.color = Color.green;
        FindClosestEnemy();
        Invoke("ResetTarget", brainwashTime);
    }

    public void ResetTarget()
    {
        hasTarget = true;
        isBrainwashed = false;
        Destroy(mindControl);
        target = GameObject.FindGameObjectWithTag("Player").transform;
        layer = LayerMask.GetMask("Player");
        targetEntity = target.GetComponent<LivingEntity>();
        targetEntity.OnDeath += OnTargetDeath;
    }

    private void FindClosestEnemy()
    {
        LayerMask layerWall = LayerMask.GetMask("Wall");
        RaycastHit rh;

        Collider[] colliders = Physics.OverlapSphere(transform.position, 7, brainwashCollision);

        if (colliders.Length <= 1)
            target = transform;

        foreach (Collider hit in colliders)
        {
            if (Physics.Raycast(transform.position, hit.transform.position - transform.position, out rh, Mathf.Infinity))
            {
                if (rh.collider.gameObject.layer != Mathf.Log(layerWall, 2))
                {
                    hasTarget = true;
                    target = hit.transform;
                    targetEntity = target.GetComponent<LivingEntity>();
                    layer = brainwashCollision;
                    targetEntity.OnDeath += OnTargetDeath;
                    return;
                }
                else
                    target = transform;
            }
        }
    }
}
