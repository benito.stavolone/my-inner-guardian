﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;
using JetBrains.Annotations;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyFollowerController : Enemy, IBurnable, IMinigable, IBrainWashed
{
    public enum State { Idle, Chasing, Attacking};

    State currentState;

    public float damage = 10;
    public float speed = 6;
    public float distanceFromPlayer = 25;

    [SerializeField] private GameObject lifeBar;

    [Header("BurnEffect")]
    public float burnDamage =1;
    public float burnTime = 5;
    private float currentBurnTime;

    [Header("BrainwashEffect")]
    public float brainwashTime = 5;

    public LayerMask brainwashCollision;
    public LayerMask layer;

    private NavMeshAgent pathfinder;
    private Transform target;
    private bool hasTarget;
    private LivingEntity targetEntity;
    private float attackDistanceThreshold = 1.5f;
    private float timeBetweenAttacks = 1;
    private bool isBrainwashed = false;

    private float nextAttackTime;

    private float myCollisionRadius;
    private float targetCollisionradius;
    private bool isVisible = false;
    private float initialSpeed;

    private RaycastHit hit;

    private LivingEntity healthEntity;

  

    private MiniGameController _miniGameController;

  
    protected override void Start()
    {
        base.Start();
        pathfinder = GetComponent<NavMeshAgent>();

        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            _miniGameController = FindObjectOfType<MiniGameController>();
            target = GameObject.FindGameObjectWithTag("Player").transform;
            hasTarget = true;
            currentState = State.Chasing;
            targetEntity = target.GetComponent<LivingEntity>();
            targetEntity.OnDeath += OnTargetDeath;
            healthEntity = transform.GetComponent<LivingEntity>();
            myCollisionRadius = GetComponent<CapsuleCollider>().radius;
            targetCollisionradius = target.GetComponent<CapsuleCollider>().radius;
            currentBurnTime = burnTime;
            initialSpeed = speed;
        }       
    }

    private void Update()
    {
        if (GameManager.instance.Isminigamerunning)
        {
            return;
        }
        else
        {
            speed = initialSpeed;
            if (isVisible)
            {
                if (hasTarget)
                {
                    if (hasTarget && Vector3.Distance(transform.position, target.position) < distanceFromPlayer)
                    {
                        transform.LookAt(target);
                        StartCoroutine(SearchPlayer());
                    }
                }
                if (!hasTarget && isBrainwashed)
                    FindClosestEnemy();
            }
        }     
    }

    IEnumerator SearchPlayer()
    {
        float refreshRate = 1.5f;
        LayerMask layerWall = LayerMask.GetMask("Wall");

        if (Physics.Raycast(transform.position, transform.forward, out hit, distanceFromPlayer))
        {
            if (hit.collider.gameObject.layer == Mathf.Log(layer.value, 2) || lifeBar.activeSelf)
            {
                pathfinder.speed = speed;
                lifeBar.SetActive(true);
                StartCoroutine(UpdatePath());

                if (Time.time > nextAttackTime)
                {
                    float sqrtDistancetoTarget = (target.position - transform.position).sqrMagnitude;
                    if (sqrtDistancetoTarget < Mathf.Pow(attackDistanceThreshold + myCollisionRadius + targetCollisionradius, 2f))
                    {
                        nextAttackTime = Time.time + timeBetweenAttacks;
                        if (Physics.Raycast(transform.position, transform.forward, out hit, 3f))
                        {
                            if (hit.collider.gameObject.layer != 10)
                                StartCoroutine(Attack());
                        }
                    }
                }
                else
                    pathfinder.speed = 0;
            }
        }
        yield return new WaitForSeconds(refreshRate);
    }

    IEnumerator Attack()
    {
        pathfinder.enabled = false;
        currentState = State.Attacking;

        Vector3 originalPosition = transform.position;
        Vector3 attackPosition = target.position;

        float attackSpeed = 3;
        float percent = 0;

        bool hasAppliedDamage = false;

        while (percent <= 1)
        {
            if(percent > 0.5f && !hasAppliedDamage)
            {
                hasAppliedDamage = true;
                targetEntity.TakeDamage(damage);
            }
            percent += Time.deltaTime * attackSpeed;
            float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;
            transform.position = Vector3.Lerp(originalPosition, attackPosition, interpolation);
            yield return null;
        }
        currentState = State.Chasing;
        pathfinder.enabled = true;
    }

    IEnumerator UpdatePath()
    {
        float refreshRate = 0;
        while (hasTarget)
        {
            if (currentState == State.Chasing)
            {
                Vector3 directionTotarget = (target.position - transform.position).normalized;
                Vector3 targetPosition = target.position - directionTotarget * (myCollisionRadius + targetCollisionradius + attackDistanceThreshold / 2);
                if (dead == false)
                {
                    if (Vector3.Distance(target.position, this.transform.position) < distanceFromPlayer)
                    {
                        refreshRate = 1f;
                        pathfinder.SetDestination(targetPosition);
                    }
                    else
                    {
                        //refreshRate = 1f;
                        //Vector3 newPath = FindNewPath(0.01f);
                        //pathfinder.SetDestination(newPath);
                    }
                }
            }
            yield return new WaitForSeconds(refreshRate);
        }
    }

    private Vector3 FindNewPath(float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        if(NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPosition = hit.position;
        }
        return finalPosition;
    }

    private void OnTargetDeath()
    {
        hasTarget = false;
        currentState = State.Idle;
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }

    public void Burning()
    {
        if (currentBurnTime > 0)
        {
            healthEntity.TakeDamage(burnDamage);
            currentBurnTime--;
            Invoke("Burning", 1);
        }
        else
        {
            Destroy(fire);
            currentBurnTime = burnTime;
        }
    }

    public void Hacking()
    {
        _miniGameController.StartRandomMinigame(gameObject);
        speed = 0;
    }

    public void ChangeTarget()
    {
        isBrainwashed = true;
        transform.GetComponent<MeshRenderer>().material.color = Color.green;
        FindClosestEnemy();
        Invoke("ResetTarget", brainwashTime);
    }

    public void ResetTarget()
    {
        isBrainwashed = false;
        hasTarget = true;
        Destroy(mindControl);
        target = GameObject.FindGameObjectWithTag("Player").transform;
        targetEntity = target.GetComponent<LivingEntity>();
        layer = LayerMask.GetMask("Player");
        targetCollisionradius = target.GetComponent<CapsuleCollider>().radius;
        targetEntity.OnDeath += OnTargetDeath;
    }

    private void FindClosestEnemy()
    {
        LayerMask layerWall = LayerMask.GetMask("Wall");
        RaycastHit rh;

        Collider[] colliders = Physics.OverlapSphere(transform.position, 7, brainwashCollision);

        if (colliders.Length <= 1)
            target = transform;

        foreach(Collider hit in colliders)
        {
            if (Physics.Raycast(transform.position, hit.transform.position - transform.position, out rh, Mathf.Infinity))
            {
                if (rh.collider.gameObject.layer != Mathf.Log(layerWall, 2))
                {
                    hasTarget = true;
                    target = hit.transform;
                    targetCollisionradius = target.GetComponent<CapsuleCollider>().radius;
                    targetEntity = target.GetComponent<LivingEntity>();
                    layer = brainwashCollision;
                    targetEntity.OnDeath += OnTargetDeath;
                    return;
                }
                else
                    target = transform;
            }
        }
    }
}
