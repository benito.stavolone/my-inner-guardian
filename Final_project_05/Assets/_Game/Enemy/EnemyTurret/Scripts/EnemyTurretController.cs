﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyTurretController : Enemy, IBurnable, IMinigable, IBrainWashed
{
    public float startTimeBtwShots;
    public LayerMask layer;
    [SerializeField] private GameObject lifeBar;

    private Transform target;
    private float timeBetweenShots;
    private LivingEntity targetEntity;
    private LivingEntity healthEntity;

    private RaycastHit hit;

    public Gun equipedGun;
    public float distanceFromPlayer=15;

    private bool hasTarget;
    private bool isVisible = false;

    [Header("BurnEffect")]
    public float burnDamage = 1;
    public float burnTime = 5;
    private float currentBurnTime;

    [Header("BrainwashEffect")]
    private bool isBrainwashed = false;
    public float brainwashTime = 5;
    public LayerMask brainwashCollision;

    private MiniGameController _miniGameController;

    private void Awake()
    {
        _miniGameController = FindObjectOfType<MiniGameController>();
    }

   
    protected override void Start()
    {
        base.Start();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        targetEntity = target.GetComponent<LivingEntity>();
        targetEntity.OnDeath += OnTargetDeath;
        hasTarget = true;
        timeBetweenShots = startTimeBtwShots;
        healthEntity = transform.GetComponent<LivingEntity>();
        currentBurnTime = burnTime;
    }


    void Update()
    {
        if (GameManager.instance.Isminigamerunning)
        {
            return;
        }
        if (isVisible)
        {
            if (hasTarget && Vector3.Distance(transform.position, target.position) < distanceFromPlayer)
                StartCoroutine(SearchPlayer());

            if (!hasTarget && isBrainwashed)
            {
                FindClosestEnemy();
            }
        }
    }

    IEnumerator SearchPlayer()
        {
            int refreshrate = 2;

            StartCoroutine(UpdateTarget());
            if (Physics.Raycast(transform.position, transform.forward, out hit, distanceFromPlayer))
            {
                if (hit.collider.gameObject.layer == Mathf.Log(layer.value, 2))
                {
                    lifeBar.SetActive(true);
                    equipedGun.OnTriggerHold();
                }
            }
            yield return new WaitForSeconds(refreshrate);
        }      
    

    IEnumerator UpdateTarget()
    {
        float refreshrate = 1f;
        transform.LookAt(target);
        yield return new WaitForSeconds(refreshrate);
    }

    private void OnTargetDeath()
    {
        hasTarget = false;
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }

    public void Burning()
    {
        if (currentBurnTime > 0)
        {
            healthEntity.TakeDamage(burnDamage);
            currentBurnTime--;
            Invoke("Burning", 1);
        }
        else
        {
            Destroy(fire);
            currentBurnTime = burnTime;
        }
            
    }

    public void Hacking()
    {
        _miniGameController.StartRandomMinigame(this.gameObject);
    }

    public void ChangeTarget()
    {
        isBrainwashed = true;
        transform.GetComponent<MeshRenderer>().material.color = Color.green;
        FindClosestEnemy();
        Invoke("ResetTarget", brainwashTime);
    }

    public void ResetTarget()
    {
        hasTarget = true;
        isBrainwashed = false;
        Destroy(mindControl);
        target = GameObject.FindGameObjectWithTag("Player").transform;
        layer = LayerMask.GetMask("Player");
        equipedGun.SetCollisionMask(LayerMask.GetMask("Player"));
        targetEntity = target.GetComponent<LivingEntity>();
        targetEntity.OnDeath += OnTargetDeath;
    }

    private void FindClosestEnemy()
    {
        LayerMask layerWall = LayerMask.GetMask("Wall");
        RaycastHit rh;

        Collider[] colliders = Physics.OverlapSphere(transform.position, 7, brainwashCollision);

        if (colliders.Length <= 1)
            target = transform;

        foreach (Collider hit in colliders)
        {
            if (Physics.Raycast(transform.position, hit.transform.position - transform.position, out rh, Mathf.Infinity))
            {
                if (rh.collider.gameObject.layer != Mathf.Log(layerWall, 2))
                {
                    hasTarget = true;
                    target = hit.transform;
                    targetEntity = target.GetComponent<LivingEntity>();
                    layer = brainwashCollision;
                    equipedGun.SetCollisionMask(brainwashCollision);
                    targetEntity.OnDeath += OnTargetDeath;
                    return;
                }
                else
                    target = transform;
            }
        }
    }
}
