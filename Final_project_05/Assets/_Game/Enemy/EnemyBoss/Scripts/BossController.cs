﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;

public class BossController : Enemy, IBurnable, IMinigable
{
    [SerializeField] private BossColliderTrigger colliderTrigger;
    [SerializeField] private Projectile projectile;
    [SerializeField] private LayerMask collisionMask;

    [Header("Material")]

    [SerializeField] private Material materialReference;
    private Color initialMaterial;

    [Header("Arm Reference")]

    [SerializeField] PendulumAnimationController[] armReference;

    [SerializeField] private GameObject doorToOpen;

    [Header("InBattle")]
    [SerializeField] private LayerMask layer;
    public PowerPerStage[] StagePower = new PowerPerStage[4];
    int powerStageIndex = 0;

    [Header("BurnEffect")]
    public float burnDamage = 1;
    public float burnTime = 5;
    private float currentBurnTime;

    private LivingEntity targetEntity;
    private Transform target;
    private bool hasTarget;
    private bool isStatic = false;

    public GameObject meshStatic;

    private float timeBetweenShots;
    private float nextShotTime;
    private int indexStage = 0;
    private bool inShooting = false;
    private bool isVisible = false;
    private LivingEntity healthEntity;
    private int projectileRemaining;

    public System.Action Damage;

    private MiniGameController _miniGameController;

    private void Awake()
    {
        _miniGameController = FindObjectOfType<MiniGameController>();
    }

  
    protected override void Start()
    {
        base.Start();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        targetEntity = target.GetComponent<LivingEntity>();
        targetEntity.OnDeath += OnTargetDeath;
        hasTarget = true;
        timeBetweenShots = StagePower[powerStageIndex].startTimeBtwShots;
        colliderTrigger.OnPlayerEnterTrigger += ColliderTrigger_OnPlayerEnterTrigger;
        healthEntity = transform.GetComponent<LivingEntity>();
        currentBurnTime = burnTime;
        initialMaterial = materialReference.color;
    }

    private void Update()
    {
        if (GameManager.instance.Isminigamerunning)
        {
            projectileRemaining = 0;
            return;
        }

        if (isVisible)
        {
            if (!isStatic)
            {
                if (!inShooting)
                {
                    Shot();
                }
                else
                    StartCoroutine(LookPlayer());
            }
        }
    }

    IEnumerator LookPlayer()
    {
            transform.LookAt(target);
            yield return new WaitForSeconds(4);
    }

    private void Shot()
    {
        for (int i = 0; i < StagePower[indexStage].projectileSpawn.Length; i++)
        {
            if (projectileRemaining == 0)
                break;

            projectileRemaining--;
            nextShotTime = Time.time + StagePower[indexStage].msBetweenShots / 1000;
            Projectile newProjectile = Instantiate(projectile, StagePower[indexStage].projectileSpawn[i].position, StagePower[indexStage].projectileSpawn[i].rotation) as Projectile;
            newProjectile.SetCollisionMask(collisionMask);
            newProjectile.SetSpeed(StagePower[indexStage].muzzleVelocity);
        }  

        if (projectileRemaining == 0)
        {
            inShooting = true;
            Invoke("Reload", StagePower[indexStage].reloadTime);
        }
    }

    private void Reload()
    {
        projectileRemaining = StagePower[indexStage].projectilePerMag;
        inShooting = false;
    }

    private void OnTargetDeath()
    {
        hasTarget = false;
    }

    public override void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        base.TakeHit(damage, hitPoint, hitDirection);
        Damage?.Invoke();
    }

    private void ColliderTrigger_OnPlayerEnterTrigger(object sender, System.EventArgs e)
    {
        colliderTrigger.OnPlayerEnterTrigger -= ColliderTrigger_OnPlayerEnterTrigger;
        projectileRemaining = StagePower[indexStage].projectilePerMag;
        isVisible = true;
    }

    public void StaticPhase()
    {
        materialReference.SetColor("_EmissionColor", Color.red*1);
        ActiveArms(false);
        isStatic = true;
        indexStage++;
        OnStageChanged();
    }

    private void ResetStaticPhase()
    {
        materialReference.SetColor("_EmissionColor", initialMaterial);
        isStatic = false;
        ActiveArms(true);
        gameObject.layer = 8;
    }

    private void OnStageChanged()
    {
        timeBetweenShots = StagePower[powerStageIndex].startTimeBtwShots;
        gameObject.layer = 10;
        Invoke("ResetStaticPhase", 5);
    }

    public void Burning()
    {
        if (currentBurnTime > 0)
        {
            healthEntity.TakeDamage(burnDamage);
            currentBurnTime--;
            Invoke("Burning", 1);
        }
        else
        {
            Destroy(fire);
            currentBurnTime = burnTime;
        }
            
    }

    public void Hacking()
    {
        _miniGameController.StartRandomMinigame(this.gameObject);
    }

    protected override void OnKill()
    {
        base.OnKill();
        slider.gameObject.SetActive(false);
        Destroy(doorToOpen);
    }

    private void ActiveArms(bool value)
    {
        for (int i = 0; i < armReference.Length; i++)
            armReference[i].enabled = value;
    }

}

[System.Serializable]
public class PowerPerStage
{
    public float muzzleVelocity;
    public float startTimeBtwShots;
    public float reloadTime;
    public int projectilePerMag;
    public float msBetweenShots;
    public Transform[] projectileSpawn;
}
