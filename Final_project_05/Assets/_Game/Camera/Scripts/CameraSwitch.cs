﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    public GameObject cam1;
    public GameObject cam2;

    private void OnCollisionStay(Collision collision)
    {
       CameraController();
    }

    public void CameraController()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            cam1.SetActive(false);
            cam2.SetActive(true);
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            cam1.SetActive(true);
            cam2.SetActive(false);
        }
    }  

   
}
