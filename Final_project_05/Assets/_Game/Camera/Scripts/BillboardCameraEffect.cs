﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardCameraEffect : MonoBehaviour
{

    private Camera mainCamera;
    private bool isVisible = true;


    void Start()
    {
        mainCamera = Camera.main;
    }


    void Update()
    {
        if(isVisible)
            transform.LookAt(transform.position - mainCamera.transform.rotation * Vector3.back, mainCamera.transform.rotation * Vector3.up);
    }

    private void OnBecameInvisible()
    {
        isVisible = false;
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }
}
