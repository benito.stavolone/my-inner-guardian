﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DescendController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if (other.GetComponent<PlayerController>().CheckIsGrounded == false)
            {
                other.gameObject.GetComponent<PlayerController>().CheckIsGrounded = true;
                other.gameObject.GetComponent<PlayerController>().isTimeToFlashTorch(true);
                other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
            }
               
        }
    }
}
