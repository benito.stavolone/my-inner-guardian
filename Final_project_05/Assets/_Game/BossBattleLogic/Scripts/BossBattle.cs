﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossBattle : MonoBehaviour
{
    public enum Stage
    {
        WaitingToStart,
        Stage1,
        Stage2,
        Stage3,
        FinalStage
    }

    [SerializeField] BossColliderTrigger colliderTrigger;
    [SerializeField] GameObject[] pfEnemySpawn;
    [SerializeField] BossController enemyBoss;
    [SerializeField] LivingEntity bossEntity;
    [SerializeField] private GameObject entrance;
    [SerializeField] private Slider bossSlider;

    private List<Transform> spawnPositionList;
    private GameManager gameManagerRef;

    private Stage stage;

    private void Awake()
    {
        spawnPositionList = new List<Transform>();
        foreach (Transform spawnPosition in transform.Find("SpawnPosition"))
        {
            spawnPositionList.Add(spawnPosition);
        }

        stage = Stage.WaitingToStart;
    }

    private void Start()
    {
        colliderTrigger.OnPlayerEnterTrigger += ColliderTrigger_OnPlayerEnterTrigger;
        gameManagerRef = GameManager.instance;
        enemyBoss.Damage += BossDamaged;
    }

    private void ColliderTrigger_OnPlayerEnterTrigger(object sender, System.EventArgs e)
    {
        StartBattle();
        bossSlider.gameObject.SetActive(true);
        colliderTrigger.OnPlayerEnterTrigger -= ColliderTrigger_OnPlayerEnterTrigger;
        entrance.SetActive(true);
        entrance.layer = 10;
    }

    private void BossDamaged()
    {
        switch (stage)
        {
            default:
            case Stage.Stage1:
                if (bossSlider.value < 0.7)
                    StartNextStage();
                break;
            case Stage.Stage2:
                if (bossSlider.value < 0.4)
                    StartNextStage();
                break;
            case Stage.Stage3:
                if (bossSlider.value < 0.1)
                    StartNextStage();
                break;
            case Stage.FinalStage:
                break;
        }
    }

    private void StartBattle()
    {   
        StartNextStage();
        var audiomanager = FindObjectOfType<AudioManager>();
        audiomanager.Play("Boss");       
       
    }

    private void SpawnEnemy()
    {
        for (int i = 0; i < spawnPositionList.Count; i++)
        {
            int probabilitytoSpawn = Random.Range(0, 101);
            if (probabilitytoSpawn < gameManagerRef._bossbattle *6 +10)
            {
                Instantiate(pfEnemySpawn[Random.Range(0, pfEnemySpawn.Length)], spawnPositionList[i].position, Quaternion.identity);
            }
        }
    }

    private void StartNextStage()
    {
        switch (stage)
        {
            default:
            case Stage.WaitingToStart:
                stage = Stage.Stage1;
                break;
            case Stage.Stage1:
                stage = Stage.Stage2;
                enemyBoss.StaticPhase();
                break;
            case Stage.Stage2:
                stage = Stage.Stage3;
                enemyBoss.StaticPhase();
                break;
            case Stage.Stage3:
                stage = Stage.FinalStage;
                enemyBoss.StaticPhase();
                break;
            case Stage.FinalStage:
                break;
        }

        SpawnEnemy();
    }
}
