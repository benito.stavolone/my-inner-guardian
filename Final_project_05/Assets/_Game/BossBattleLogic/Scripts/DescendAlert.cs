﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DescendAlert : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(other.GetComponent<PlayerController>().CheckIsGrounded == true)
            {
                other.GetComponent<PlayerController>().CheckIsGrounded = false;
                other.gameObject.GetComponent<PlayerController>().isTimeToFlashTorch(false);
                other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
            }
        }
    }
}
