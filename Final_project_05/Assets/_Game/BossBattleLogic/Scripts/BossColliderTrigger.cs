﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BossColliderTrigger : MonoBehaviour
{

    public event EventHandler OnPlayerEnterTrigger;

    private void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        if (player != null)
        {
            OnPlayerEnterTrigger?.Invoke(this, EventArgs.Empty);
        }
    }
}
