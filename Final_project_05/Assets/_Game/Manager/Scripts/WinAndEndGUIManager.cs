﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WinAndEndGUIManager : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private GameObject objectToActivate;

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.useController)
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
        else
        {
            if (EventSystem.current.currentSelectedGameObject == null)
                EventSystem.current.SetSelectedGameObject(objectToActivate);
        }
    }
}
