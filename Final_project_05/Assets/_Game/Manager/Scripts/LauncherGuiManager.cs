﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class LauncherGuiManager : MonoBehaviour
{
    [Header("Panel List")]

    public GameObject StartPanel;
    public GameObject ControlsPanel;
    public GameObject CreditsPanel;
    public GameObject OptionPanel;


    [Header("First Button For Each Panel")]

    public GameObject firstStartButton;
    public GameObject firsControlButton;
    public GameObject firstCreditButton;
    public GameObject firstOptionButton;
   
    private void EventRegister()
    {
        GameManager.instance.ChangeGameStateEvent += RefreshPanel;
    }

    private void EventUnRegister()
    {
        GameManager.instance.ChangeGameStateEvent -= RefreshPanel;      
    }

    private void Start()
    {
        GameManager.instance.gameState = GameState.LAUNCHER;
        EventRegister();
    }

    private void OnDestroy()
    {
        EventUnRegister();
    }

    public void RefreshPanel(GameState gamestate)
    {
        switch (gamestate)
        {
            case GameState.LAUNCHER:
                ActiveStartPanel();
                break;

            case GameState.OPTIONS:
                ActiveOptionPanel();
                break;

            case GameState.CONTROLS:
                ActiveControlsPanel();
                break;

            case GameState.CREDITS:
                ActiveCreditsPanel();
                break;

        }
    }

    public void OnClickButtonStart()
    {
        GameManager.instance.GamePlay();
    }

    public void OnClickControlButton()
    {
        GameManager.instance.Controls();

        EventSystem.current.SetSelectedGameObject(null);
    }

    public void OnClickCreditsButton()
    {
        GameManager.instance.Credits();       
    }

    public void OnClickQuitButton()
    {
        GameManager.instance.Quit();
    }

    public void OnClickOptions()
    {
        GameManager.instance.Options();
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void OnClickBack()
    {
        StartPanel.SetActive(true);
        ControlsPanel.SetActive(false);
        CreditsPanel.SetActive(false);
        OptionPanel.SetActive(false);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstStartButton);
    }


    public void ActiveStartPanel()
    {
        StartPanel.SetActive(true);
        ControlsPanel.SetActive(false);
        CreditsPanel.SetActive(false);
        OptionPanel.SetActive(false);
    }


    public void ActiveControlsPanel()
    {
        StartPanel.SetActive(false);
        ControlsPanel.SetActive(true);
        CreditsPanel.SetActive(false);
        OptionPanel.SetActive(false);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firsControlButton);
    }

    public void ActiveCreditsPanel()
    {
        StartPanel.SetActive(false);
        ControlsPanel.SetActive(false);
        CreditsPanel.SetActive(true);
        OptionPanel.SetActive(false);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstCreditButton);
    }

    public void ActiveOptionPanel()
    {
        StartPanel.SetActive(false);
        ControlsPanel.SetActive(false);
        CreditsPanel.SetActive(false);
        OptionPanel.SetActive(true);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstOptionButton);
    }

    private void Update()
    {
        if (StartPanel.activeSelf)
            CheckController(firstStartButton);

        if (OptionPanel.activeSelf)
            CheckController(firstOptionButton);

        if (CreditsPanel.activeSelf)
            CheckController(firstCreditButton);

        if (ControlsPanel.activeSelf)
            CheckController(firsControlButton);
    }

    private void CheckController(GameObject objectToActivate)
    {
        if (!GameManager.instance.useController)
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
        else
        {
            if (EventSystem.current.currentSelectedGameObject == null)
                EventSystem.current.SetSelectedGameObject(objectToActivate);
        }
    }




}
