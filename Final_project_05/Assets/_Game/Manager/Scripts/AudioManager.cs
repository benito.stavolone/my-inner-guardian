﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public List<Sound> sounds;
    public AudioMixerGroup mixer;
    private AudioSource _audioSource;

    private void Awake()
    {
        sounds.ForEach(sto => Debug.Log(sto.name));
        _audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        Play("Main");
        //Per richiamare i suoni negli script " FindObjectOfType<AudioManager>().Play("nomedelsuono") "
    }
    public void Play(string name)
    {
        Debug.Log(sounds.Any(so => so.name == name));
        Sound s = sounds.Single(so => so.name == name);
        sounds.ForEach(sto => Debug.Log(sto.name));
        _audioSource.clip = s.clip;
        _audioSource.volume = s.volume;
        _audioSource.pitch = s.pitch;
        _audioSource.loop = s.loop;
        _audioSource.Play();
    }

}
