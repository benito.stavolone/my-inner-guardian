﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;

public class GameManager : PeristentSingleton<GameManager>
{
    public event Action<GameState> ChangeGameStateEvent;
    public GameObject loadingScreen;
    public int _bossbattle = 0;
    public Slider slider;

    [SerializeField] private GameObject pausePanelFirstSelectedButton;
    
    [HideInInspector] public bool useController = false;

    public bool Isminigamerunning
    {
        get;
        set;
    }

    [SerializeField] private GameState _gameState;
    public GameState gameState
    {
        get
        {
            return _gameState;
        }
        set
        {
            _gameState = value;
            if(value == GameState.PAUSE)
            {
                Time.timeScale = 0;
            }
            
            else
            {
                Time.timeScale = 1;
            }
            ChangeGameStateEvent?.Invoke(value);
            
        }
    }

    private float _timeScale = 1;

    private void Start()
    {
        StartCoroutine(JoystickCheck());
    }

    private void Update()
    {
        if (!useController)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Pause();
            }
        }

        if (useController)
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button7))
                Pause();
        }

    }
    public void Launcher()
    {
        gameState = GameState.LAUNCHER;
        SceneManager.LoadScene(0);
    }

    public void Controls()
    {
        gameState = GameState.CONTROLS;
    }

    public void Credits()
    {
        gameState = GameState.CREDITS;
    }

    public void GamePlay()
    {
        gameState = GameState.GAMEPLAY;
        StartCoroutine(LoadAsynchronously(1));
    }

    public void Pause()
    {           
            if(gameState == GameState.GAMEPLAY)
            {
                gameState = GameState.PAUSE;
            }
            else if(gameState == GameState.PAUSE)
            {
                gameState = GameState.GAMEPLAY;         
            }
    }
    public void Win()
    {
        gameState = GameState.WIN;
        SceneManager.LoadScene(2);
    }
    public void Lose()
    {
        gameState = GameState.LOSE;
        SceneManager.LoadScene(2);
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void GameOver()
    {
        gameState = GameState.GAMEOVERLOSE;
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
        StartCoroutine(LoadAsynchronously(1));
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
        gameState = GameState.LAUNCHER;
    }

    public void Options()
    {
        gameState = GameState.OPTIONS;
    }

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        loadingScreen.SetActive(true);

        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            yield return null;
        }

        loadingScreen.SetActive(false);

    }

    IEnumerator JoystickCheck()
    {
        while (true)
        {
            for(int i=0; i< Input.GetJoystickNames().Length; i++)
            {
                if (!string.IsNullOrEmpty(Input.GetJoystickNames()[i]))
                {
                    i = Input.GetJoystickNames().Length;
                    useController = true;
                    Cursor.visible = false;
                }
                else
                {
                    i = Input.GetJoystickNames().Length;
                    useController = false;
                    Cursor.visible = true;
                }
            }
            yield return new WaitForSecondsRealtime(2);
        }
    }




}
