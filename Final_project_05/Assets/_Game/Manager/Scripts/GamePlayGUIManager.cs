﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GamePlayGUIManager : MonoBehaviour
{
    public static GamePlayGUIManager instance;

    [Header("Panel")]
    public GameObject gamePlayPanel;
    public GameObject minigamePanel;

    public TextMeshProUGUI maxAmmoPerGun;
    public TextMeshProUGUI currentAmmoPerGun;

    public Image currentTypeofPower;
    public Image typeOfPower1;
    public Image typeOfPower2;

    public Image currentTypeofGun;

    public TextMeshProUGUI currentAmmoofPower;

    public event System.Action<bool> OnMinigameStarted;

    private void Awake()
    {
        instance = this;
    }
    public void RefreshMaxAmmoPerGun(int value)
    {
        maxAmmoPerGun.text = " / " + value.ToString();
    }

    public void RefreshCurrentAmmoPerGun(int value)
    {
        currentAmmoPerGun.text = value.ToString();
    }

    public void RefreshCurrentGunImage(Image newSprite)
    {
        currentTypeofGun.sprite = newSprite.sprite;
        currentTypeofGun.color = newSprite.color;
    }

    public void RefreshCurrentTypeOfPower(Image newSprite)
    {
        currentTypeofPower.sprite = newSprite.sprite;
        currentTypeofPower.color = newSprite.color;
    }

    public void RefreshTypeOfPower1(Image newSprite, int index)
    {
        if (index == 0)
        {
            typeOfPower1.sprite = newSprite.sprite;
            typeOfPower1.color = newSprite.color;
        }
        else
        {
            typeOfPower2.sprite = newSprite.sprite;
            typeOfPower2.color = newSprite.color;
        }
            
    }

    public void RefreshAmmoOfPower(int value)
    {
        currentAmmoofPower.text = value.ToString();
    }

    public void ActiveMiniGamePanel()
    {
        gamePlayPanel.SetActive(false);
        minigamePanel.SetActive(true);
        OnMinigameStarted?.Invoke(true);
    }

    public void DisactiveMiniGamePanel()
    {
        gamePlayPanel.SetActive(true);
        minigamePanel.SetActive(false);
        OnMinigameStarted?.Invoke(false);
    }
}


