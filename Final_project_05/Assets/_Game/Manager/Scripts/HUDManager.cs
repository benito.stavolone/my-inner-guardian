﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Authentication.ExtendedProtection;
using UnityEngine;
using UnityEngine.EventSystems;

public class HUDManager : MonoBehaviour
{
    public GameObject PausePanel;
    public GameObject firstButtonSelected;

    private void EventRegister()
    {
        GameManager.instance.ChangeGameStateEvent += RefreshPanel;
    }

    private void EventUnRegister()
    {
        GameManager.instance.ChangeGameStateEvent -= RefreshPanel;
    }

    private void Start()
    {
        GameManager.instance.gameState = GameState.GAMEPLAY;
        EventRegister();
    }

    private void OnDestroy()
    {
        EventUnRegister();
    }

    public void RefreshPanel(GameState gamestate)
    {
        switch(gamestate)
        {
            case GameState.GAMEPLAY:
                PausePanel.SetActive(false);
                EventSystem.current.SetSelectedGameObject(null);
                break;

            case GameState.PAUSE:
                PausePanel.SetActive(true);
                if(GameManager.instance.useController)
                    EventSystem.current.SetSelectedGameObject(firstButtonSelected);
                break;

        }
    }

    private void Update()
    {
        if (PausePanel.activeSelf)
            CheckController(firstButtonSelected);
    }

    public void BackToMenu()
    {
        GameManager.instance.BackToMenu();
        GameManager.instance.Isminigamerunning = false;
    }

    public void Continue()
    {
        GameManager.instance.Pause();
    }
    
    public void Restart()
    {
        GameManager.instance.Restart();
        GameManager.instance.Isminigamerunning = false;
    }

    public void Quit()
    {
        Application.Quit();
    }

    private void CheckController(GameObject objectToActivate)
    {
        if (!GameManager.instance.useController)
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
        else
        {
            if (EventSystem.current.currentSelectedGameObject == null)
                EventSystem.current.SetSelectedGameObject(objectToActivate);
        }
    }
}
