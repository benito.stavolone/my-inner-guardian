﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [SerializeField] private float speed;
    private void Update()
    {
        Rotation();
    }

    private void Rotation()
    {
        transform.Rotate(0,0,10 * Time.deltaTime * speed);
    }
}
