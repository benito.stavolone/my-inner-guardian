﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearsAndDisappears : MonoBehaviour
{
    [SerializeField] private float timer;
    [SerializeField] private GameObject meshObject;
    private void Update()
    {
        Appears();
    }

    private void Appears()
    {
        if(timer == 2)
        {
            MeshRenderer m = meshObject.GetComponent<MeshRenderer>();
            m.enabled = true;
        }
        else
        {
            MeshRenderer m = meshObject.GetComponent<MeshRenderer>();
            m.enabled = false;
        }
    }
}
