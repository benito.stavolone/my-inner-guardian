﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpAndDown : MonoBehaviour
{
    [SerializeField] private Animator Obstacle;

    private void Awake()
    {
        Obstacle = GetComponent<Animator>();
    }
    private void OnEnable()
    {
        Obstacle.SetBool("obstacle",true);
    }

    private void OnDisable()
    {
        Obstacle.SetBool("obstacle",false);
    }
}
   


