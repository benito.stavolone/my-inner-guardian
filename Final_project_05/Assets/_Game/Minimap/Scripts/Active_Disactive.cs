﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Active_Disactive : MonoBehaviour
{
    [SerializeField] private GameObject minimap;
    private void Update()
    {
        ActiveMinimap(); 
    }
    
    private void ActiveMinimap()
    {
       if(Input.GetKeyDown(KeyCode.M))
        {
            minimap.SetActive(!minimap.activeInHierarchy);
        }
    }
}
