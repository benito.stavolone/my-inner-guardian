﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{
    public enum FireMode { Auto, Burst, Single };
    public FireMode fireMode;

    public Transform[] projectileSpawn;
    public Projectile projectile;
    public float msBetweenShots = 100;
    public float muzzleVelocity = 35;
    public int burstCount;
    public int projectilePerMag;
    public float reloadTime = 0.5f;
    public Slider reloadSlider;
    [SerializeField] private AudioClip clip;
    private AudioSource _audioSource;

    [Header("Shell")]
    public Transform shell;
    public Transform shellEjection;

    [Header("Recoil")]
    public float kickRecoil = 0.05f;
    public float angleRecoil = 5;

    private float nextShotTime;
    private bool onTriggerReleaseSinceLastShot;
    private int shotRemaininginBurst;
    private int projectileRemaining;
    private bool isReloading;

    Vector3 recoilSmoothDampVelocity;
    float recoilAngle;
    float recoilRotSmoothDampVelocity;
    public LayerMask collisionMask;

    private MuzzleFlashController muzzleFlash;

    private bool isInstantiated = false;
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        muzzleFlash = GetComponent<MuzzleFlashController>();
        shotRemaininginBurst = burstCount;
        projectileRemaining = projectilePerMag;
        reloadSlider.value = 0;
        isInstantiated = true;
    }

    private void OnEnable()
    {
        isReloading = false;
        if (isInstantiated && projectileRemaining == 0)
        {
            StartCoroutine(AnimateReload());
        }
    }

    private void LateUpdate()
    {
        if (GameManager.instance.Isminigamerunning)
        {
            if (reloadSlider.gameObject.activeSelf)
                reloadSlider.gameObject.SetActive(false);
        }

        GamePlayGUIManager.instance.RefreshCurrentAmmoPerGun(projectileRemaining);
        transform.localPosition = Vector3.SmoothDamp(transform.localPosition, Vector3.zero, ref recoilSmoothDampVelocity, 0.1f);
        recoilAngle = Mathf.SmoothDamp(recoilAngle, 0, ref recoilRotSmoothDampVelocity, 0.1f);
        transform.localEulerAngles = transform.localEulerAngles + Vector3.right*-recoilAngle;

        if (!isReloading && projectileRemaining == 0)
            Reload();
    }

    private void Shoot()
    {

        if (!isReloading && Time.time > nextShotTime && projectileRemaining > 0)
        {          
            if (fireMode == FireMode.Burst)
            {
                if (shotRemaininginBurst == 0)
                    return;
                shotRemaininginBurst--;
            }
            else if(fireMode == FireMode.Single)
            {
                if (!onTriggerReleaseSinceLastShot)
                    return;
            }               

            for(int i = 0; i < projectileSpawn.Length; i++)
            {
                if (projectileRemaining == 0)
                    break;

                projectileRemaining--;
                nextShotTime = Time.time + msBetweenShots / 1000;
                Projectile newProjectile = Instantiate(projectile, projectileSpawn[i].position, projectileSpawn[i].rotation) as Projectile;
                newProjectile.SetCollisionMask(collisionMask);
                newProjectile.SetSpeed(muzzleVelocity);

                ObjectPool.instance.SpawnFromPool("Shell", shellEjection.position, shellEjection.rotation);

                if (collisionMask != 9)
                    muzzleFlash.Activate();                
                _audioSource.PlayOneShot(clip);
                
            }

            transform.localPosition -= Vector3.forward * kickRecoil;
            recoilAngle += angleRecoil;
            recoilAngle = Mathf.Clamp(recoilAngle, 0, 30);
        }
        
    }

    public void Reload()
    {
        if(!isReloading && projectileRemaining!=projectilePerMag)
            StartCoroutine(AnimateReload());
    }

    IEnumerator AnimateReload()
    {
        isReloading = true;
        yield return new WaitForSeconds(0.2f);

        float reloadSpeed = 1/reloadTime;
        float percent = 0;

        reloadSlider.gameObject.SetActive(true);
        reloadSlider.value = 0;

        Vector3 initialRot = transform.localEulerAngles;
        float maxReloadAngle=20;
        while (percent < 1)
        {
            percent += Time.deltaTime * reloadSpeed;
            reloadSlider.value = percent;
            float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;
            float reloadAngle = Mathf.Lerp(0, maxReloadAngle, interpolation);
            transform.localEulerAngles = initialRot + Vector3.left * reloadAngle;
            yield return null;
        }

        isReloading = false;
        reloadSlider.gameObject.SetActive(false);
        projectileRemaining = projectilePerMag;
    }

    private void OnDisable()
    {
        reloadSlider.value = 0;
        reloadSlider.gameObject.SetActive(false);
    }

    public void AimPoint(Vector3 aimPoint)
    {
        transform.LookAt(aimPoint);
    }

    public void SetCollisionMask(LayerMask layer)
    {
        collisionMask = layer;
    }

    public void OnTriggerHold()
    {
        Shoot();
        onTriggerReleaseSinceLastShot = false;
    }

    public void OnTriggerRelease()
    {
        onTriggerReleaseSinceLastShot = true;
    }
}
