﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class GunController : MonoBehaviour
{
    public Transform weaponHold;
    public List<Gun> availableGun;
    private Gun equipedGun;
    private GameManager gameManagerRef;

    private int selectedWeapon = 0;

    [Header("Power Sprite")]
    [SerializeField] private List<Image> gunsSprite;

    public float GunHeight
    {
        get
        {
            return weaponHold.position.y;
        }
    }

    private void Start()
    {
        gameManagerRef = GameManager.instance;
        SelectWeapon();
    }

    private void Update()
    {
        if (gameManagerRef.gameState == GameState.PAUSE)
            return;

        int previousSelectedWeapon = selectedWeapon;

        if (!GameManager.instance.useController)
        {
            if ((int)Input.mouseScrollDelta.y > 0)
            {
                if (selectedWeapon >= availableGun.Count - 1)
                    selectedWeapon = 0;
                else
                    selectedWeapon++;
            }
        }

        if (!GameManager.instance.useController)
        {
            if ((int)Input.mouseScrollDelta.y < 0)
            {
                if (selectedWeapon <= 0)
                    selectedWeapon = availableGun.Count - 1;
                else
                    selectedWeapon--;
            }
        }

        if (GameManager.instance.useController)
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button5))
            {
                if (selectedWeapon >= availableGun.Count - 1)
                    selectedWeapon = 0;
                else
                    selectedWeapon++;
            }
        }


        if (previousSelectedWeapon != selectedWeapon)
        {
            SelectWeapon();           
        }

        GamePlayGUIManager.instance.RefreshCurrentGunImage(gunsSprite[selectedWeapon]);
        GamePlayGUIManager.instance.RefreshMaxAmmoPerGun(equipedGun.projectilePerMag);

    }

    private void SelectWeapon()
    {
        int i = 0;
        foreach(Gun gun in availableGun)
        {
            if (i == selectedWeapon)
                gun.gameObject.SetActive(true);
            else
                gun.gameObject.SetActive(false);
            i++;
        }
        equipedGun = availableGun[selectedWeapon];
    }

    public void Aim(Vector3 aimPoint)
    {
        if (equipedGun != null)
            equipedGun.AimPoint(aimPoint);
    }

    public void OnTriggerHold()
    {
        if (equipedGun != null)
            equipedGun.OnTriggerHold();
    }

    public void OnTriggerRelease()
    {
        if (equipedGun != null)
            equipedGun.OnTriggerRelease();
    }

    public void Reload()
    {
        if (equipedGun != null)
        {
            equipedGun.Reload();
        }
    }

    public void SetMoreAmmoinMagazine(int value)
    {
        foreach(Gun g in availableGun)
        {
            g.projectilePerMag += value;
        }
        equipedGun.Reload();
    }

    public void SetNewReloadTime(float value)
    {
        foreach (Gun g in availableGun)
        {
            if(g.reloadTime>0)
                g.reloadTime -= value;
        }
    }
}
