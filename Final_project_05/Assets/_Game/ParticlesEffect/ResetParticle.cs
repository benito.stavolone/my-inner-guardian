﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetParticle : MonoBehaviour
{
    // Start is called before the first frame update
    void OnEnable()
    {
        Invoke("Reset", 1);
    }

    private void Reset()
    {
        gameObject.SetActive(false);
    }

}
