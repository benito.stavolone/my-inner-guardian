﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDropController : MonoBehaviour
{
    public int healValue = 10;
    private bool isFullofLife = false;

    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            isFullofLife =  collision.gameObject.GetComponent<LivingEntity>().UpdateHealthValue(healValue);
            if (!isFullofLife)
                Destroy(gameObject);
        }
    }
}
