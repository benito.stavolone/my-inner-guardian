﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBallMovements : MonoBehaviour
{
    public float speed;
    public GameObject spawnBall;
    
    private void OnEnable()
    {
        transform.position = spawnBall.transform.position;
       
    }
    private void Update()
    {
        transform.Translate(-new Vector2((Screen.width * 300) / 800, 0) * Time.deltaTime);
    }
}
