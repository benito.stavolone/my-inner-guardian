﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class ActivateMiniGame : MonoBehaviour
{
    private MiniGameController _miniGameController;
    private bool isInteractive = false;
    [SerializeField] private TextMeshProUGUI text;

    private void Awake()
    {
        _miniGameController = FindObjectOfType<MiniGameController>();   
    }

    private void Update()
    {
        if (!GameManager.instance.useController)
        {
            if (Input.GetKeyDown(KeyCode.E) && isInteractive)
            {

                _miniGameController.StartMinigame(this.gameObject);
                _miniGameController.DestroyTrigger(this.gameObject);

            }
        }

        if (GameManager.instance.useController)
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button0) && isInteractive)
            {
                _miniGameController.StartMinigame(this.gameObject);
                _miniGameController.DestroyTrigger(this.gameObject);
            }
        }
                
    }

    private void OnTriggerEnter(Collider other)
    {
        isInteractive = true;      
        text.gameObject.SetActive(true);

        if (GameManager.instance.useController)
            text.text = "A";
        else
            text.text = "E";
    }

    private void OnTriggerExit(Collider other)
    {
        isInteractive = false;
        text.gameObject.SetActive(false);
       
    }
}
