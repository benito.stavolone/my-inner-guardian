﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour
{
    [SerializeField] private MiniGame _minigame;
    public static event Action<MiniGameStatus,MiniGame> OnGameEnd;
}
