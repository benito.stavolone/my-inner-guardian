﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MiniGameStatus
{
    WIN,
    LOSE,
}
public class MiniGameController : MonoBehaviour
{
    private GameObject triggerminigame;
    [SerializeField] private int _bossBattle;
    [SerializeField] private List<GameObject> minigame;
    [SerializeField] private List<GameObject> enemyMinigame;

    private GameObject minigameTrigger;
    private GameManager gameManagerRef;
    private int index = 0;
    [SerializeField] AudioClip SoundPlay;
    [SerializeField] float Volume;
    AudioSource audio;
    public void MiniGameEnd(MiniGameStatus miniGameStatus, MiniGame _minigame)
    {
        if (minigameTrigger.layer != 8)
            minigame.ForEach(m => m.SetActive(false));
        else
            enemyMinigame.ForEach(m => m.SetActive(false));

        switch (miniGameStatus)
        {
            case MiniGameStatus.WIN:
                if (minigameTrigger.layer != 8)
                {
                    OnWin();
                    audio.PlayOneShot(SoundPlay,Volume);

                }
                else
                    minigameTrigger.GetComponent<Enemy>().Win(minigameTrigger);
                break;
            case MiniGameStatus.LOSE:
                if (minigameTrigger.layer != 8)
                    OnLose();
                else
                    minigameTrigger.GetComponent<Enemy>().Lose();
                break;        
        }
        GameManager.instance.Isminigamerunning = false;
    }

    private void Awake()
    {
        minigame.ForEach(m => m.SetActive(false));
        gameManagerRef = GameManager.instance;
        EventRegister();
        audio = GetComponent<AudioSource>();
    }

    private void EventRegister() 
    {
        MinigamePlayerController.OnGameEnd += MiniGameEnd;
    }
    private void EventUnregister()
    {
        MinigamePlayerController.OnGameEnd -= MiniGameEnd;
    }

    private void OnDestroy()
    {
        EventUnregister();
    }

    public void OnWin()
    {
        if(index < minigame.Count)
        {
            index++;
            Destroy(triggerminigame);
        }
    }

    public void OnLose()
    {
        GameManager.instance._bossbattle++;
    }

   public void StartMinigame(GameObject triggerMinigame)
    {
        GameManager.instance.Isminigamerunning = true;
        minigameTrigger = triggerMinigame;
        minigame[index].SetActive(true);      
    }

    public void StartRandomMinigame(GameObject triggerMinigame)
    {
        GameManager.instance.Isminigamerunning = true;
        minigameTrigger = triggerMinigame;
        int randomIndex = Random.Range(0, 3);
        enemyMinigame[randomIndex].SetActive(true);
    }

    public void DestroyTrigger(GameObject trigger)
    {
        triggerminigame = trigger;
    }
       
}