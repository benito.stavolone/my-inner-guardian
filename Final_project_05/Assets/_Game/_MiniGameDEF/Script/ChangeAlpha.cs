﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeAlpha : MonoBehaviour
{
    private Image _image;
   
    private float _maxAlpha = 255f;
    [SerializeField] private float speed = 50;


    private void Awake()
    {
        _image = GetComponent<Image>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(ChangeAlphaCorutine());
    }
    private void OnEnable()
    {
        _image.color = new Color(_image.color.r,_image.color.g,_image.color.b,0);
    }

    private void OnDisable()
    {
        StopCoroutine(ChangeAlphaCorutine());
    }
    IEnumerator ChangeAlphaCorutine()
    {
        float currentAlpha = 0f;

        while(currentAlpha < _maxAlpha)
        {
            currentAlpha += Time.deltaTime * speed;
            _image.color = new Color(_image.color.r,_image.color.g,_image.color.b,currentAlpha);
            yield return null;
        }
    }
}
