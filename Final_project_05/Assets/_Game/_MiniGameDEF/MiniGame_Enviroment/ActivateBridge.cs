﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateBridge : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private MiniGame _minigameBridge;
    [SerializeField] private float speed;
    

    private void Awake()
    {
        EventRegister();
        
    }

    private void ActivateTheBridge( MiniGameStatus miniGameStatus, MiniGame _minigame)
    {
        if(miniGameStatus == MiniGameStatus.WIN && _minigameBridge == _minigame)
        {
            float step = speed * Time.deltaTime;
            if(this.gameObject != null)
            {
                transform.position = Vector3.MoveTowards(transform.position,target.position,step);
               
            }
        }
    }

    private void OnDisable()
    {
        UnRegister();
    }

    private void EventRegister()
    {
        MinigamePlayerController.OnGameEnd += ActivateTheBridge;
    }

    private void UnRegister()
    {
        MinigamePlayerController.OnGameEnd -= ActivateTheBridge;
    }
}
