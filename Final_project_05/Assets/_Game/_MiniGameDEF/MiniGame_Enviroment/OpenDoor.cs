﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    [SerializeField] private MiniGame _minigameDoor;
    
    private void Awake()
    {
        EventRegister();
        
    }
   
    private void OpenTheDoor(MiniGameStatus miniGameStatus, MiniGame _minigame)
    {
        if(miniGameStatus == MiniGameStatus.WIN && _minigameDoor == _minigame)
        {
            if(this.gameObject != null)
            {
                this.gameObject.SetActive(false);               
            }
        }
    }

    private void OnDisable()
    {
        UnRegister();
    }

    private void EventRegister()
    {
        MinigamePlayerController.OnGameEnd += OpenTheDoor;
    }

    private void UnRegister()
    {
        MinigamePlayerController.OnGameEnd -= OpenTheDoor;
    }

}
