﻿using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottonChangeColor : MonoBehaviour
{
    private Image image;
   
    public Sprite ChangeButton;
    private Sprite StartSprite;


    private void Awake()
    {
        image = GetComponent<Image>();
    }

   
    void Start()
    {

        StartSprite = image.sprite;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        image.sprite = ChangeButton;
    }

    private void OnDisable()
    {
        image.sprite = StartSprite;
    }

}
