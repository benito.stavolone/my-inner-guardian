﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ActivatePage_Doll : MonoBehaviour
{
    public GameObject DiaryPage;
    private bool isInteractive = false;
    [SerializeField] private TextMeshProUGUI pressE;
    [SerializeField] private float speed = 1f;
    [SerializeField] private float delta = 3f;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && isInteractive)
        {
            DiaryPage.SetActive(true);
   
        }
        //if(Input.GetKeyDown(KeyCode.Escape) && isInteractive)
        //{
        //    DiaryPage.SetActive(false);
        //    Time.timeScale = 1;
        //}
   
        //Rotation();
    }

    private void OnTriggerEnter(Collider other)
    {
        //isInteractive = true;
        //pressE.gameObject.SetActive(true);
        DiaryPage.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        //isInteractive = false;
        //pressE.gameObject.SetActive(false);
        DiaryPage.SetActive(false);
    }

    private void Rotation()
    {
        transform.Rotate(0,2,0 * Time.deltaTime * speed);
    }


}
