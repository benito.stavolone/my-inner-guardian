﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateTutorial : MonoBehaviour
{
    [SerializeField] private GameObject _tutorial;
   
    private void OnTriggerEnter(Collider other)
    {
        _tutorial.SetActive(true);
       
    }

    private void OnTriggerExit(Collider other)
    {
        _tutorial.SetActive(false);
    }

   
}
