﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivingEntity : MonoBehaviour, IDamageable
{
    public float startinHealth;
    protected float health;
    protected bool dead;
    public Slider slider;

    public event System.Action OnDeath;

    protected virtual void Start()
    {
        health = startinHealth;
    }

    public virtual void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        TakeDamage(damage);
    }

    public virtual void TakeDamage(float damage)
    {
        float newDamage = startinHealth * damage / startinHealth;
        health -= newDamage;
        slider.value = health / startinHealth;
        if (health <= 0 && dead == false)
        {
            Die();
        }
    }

    public bool UpdateHealthValue(int value)
    {
        if (health >= startinHealth)
            return true;
        else
        {
            health += value;
            slider.value = health / startinHealth;
            return false;
        }
    }

    protected void Die()
    {
        dead = true;
        OnDeath?.Invoke();
        OnKill();
        GameObject.Destroy(gameObject);     
    }

    protected virtual void OnKill()
    {
    }

    protected virtual void RedBoarder()
    {

    }
}
