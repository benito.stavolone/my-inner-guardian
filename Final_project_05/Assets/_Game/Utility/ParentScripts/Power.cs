﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Power : MonoBehaviour
{
    
    public LayerMask collisionMask;

    public float speed = 15;

    private float lifetime = 5;
    private LayerMask destructionMask;


    void Start()
    {
        Collider[] initialCollision = Physics.OverlapSphere(transform.position, 0.01f, collisionMask);
        if (initialCollision.Length > 0)
        {
            OnHitObject(initialCollision[0], transform.position);
        }
        destructionMask = LayerMask.GetMask("Wall");
        Destroy(gameObject, lifetime);
    }

 
    void Update()
    {
        float moveDistance = speed * Time.deltaTime;
        CheckCollision(moveDistance);
        transform.Translate(Vector3.forward * moveDistance);
    }

    private void CheckCollision(float moveDistance)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, moveDistance, collisionMask, QueryTriggerInteraction.Collide))
        {
            OnHitObject(hit.collider, hit.point);
        }
        else if (Physics.Raycast(ray, out hit, moveDistance, destructionMask, QueryTriggerInteraction.Collide))
            Destroy(gameObject);
    }

    public virtual void OnHitObject(Collider collision, Vector3 hitPoint)
    {

    }

}
