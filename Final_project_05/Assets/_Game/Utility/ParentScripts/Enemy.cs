﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : LivingEntity
{
    [SerializeField] private float percentageToSpawnLife;

    [Header("HackerEffect")]
    [SerializeField] private float hackerDamage=5;
    
    private bool isQuitting = false;
    private bool isDead = false;
    private Vector3 hitReference;
    protected GameObject fire;
    protected GameObject mindControl;

    Transform targetPlayer;

    public GameObject dropObject;

    protected override void OnKill()
    {
        if (!isQuitting)
        {
            if(isDead==false)
                ObjectPool.instance.SpawnFromPool("EnemyDeathEffect_PS", transform.position, Quaternion.FromToRotation(Vector3.forward, hitReference));

            targetPlayer = GameObject.FindGameObjectWithTag("Player").transform;

            int spawnHealthProbability = Random.Range(0, 101);
            if (spawnHealthProbability < percentageToSpawnLife)
                Instantiate(dropObject, new Vector3(transform.position.x, targetPlayer.position.y +1, transform.position.z), Quaternion.identity);
        }
    }

    public override void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        hitReference = hitDirection;

        if (damage >= health)
        {
            ObjectPool.instance.SpawnFromPool("EnemyDeathEffect_PS", hitPoint, Quaternion.FromToRotation(Vector3.forward, hitDirection));
            isDead = true;
        }
        base.TakeHit(damage, hitPoint, hitDirection);
    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }

    public void Win(GameObject enemy)
    {
         enemy.GetComponent<LivingEntity>().TakeDamage(hackerDamage);
    }

    public void Lose()
    {
        targetPlayer = GameObject.FindGameObjectWithTag("Player").transform;
        targetPlayer.GetComponent<LivingEntity>().TakeDamage(targetPlayer.GetComponent<PlayerController>().HackerDamage);
    }

    private void OnTriggerEnter(Collider collision)
    {

        if (collision.gameObject.layer == gameObject.layer)
        {
            IBurnable burnable = collision.gameObject.GetComponent<IBurnable>();
            if (burnable != null)
            {
                burnable.Burning();
            }
        }
    }
    public void FireReference(GameObject fire)
    {
        this.fire = fire;
    }

    public void MindControlReference(GameObject mind)
    {
        this.mindControl = mind;
    }
}
