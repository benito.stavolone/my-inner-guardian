﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeristentSingleton<T> : MonoBehaviour where T : Component
{
    public static T instance { get; private set; }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this as T;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this);
        }
    }
}
