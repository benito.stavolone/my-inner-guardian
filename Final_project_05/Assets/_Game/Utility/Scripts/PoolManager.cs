﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PoolObjectType
{
    Shell
}

[System.Serializable]
public class PoolInfo
{
    public PoolObjectType type;
    public int amount = 0;
    public GameObject prefab;
    public GameObject container;

    [HideInInspector] public List<GameObject> pool = new List<GameObject>();
}

public class PoolManager : MonoBehaviour
{
    public static PoolManager instance;

    [SerializeField] List<PoolInfo> listOfPool;

    private Vector3 defaultPosition = new Vector3(-100, -100, -100);

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < listOfPool.Count; i++)
        {
            FillPool(listOfPool[i]);
        }
    }
    void FillPool(PoolInfo info)
    {
        for(int i = 0; i < info.amount; i++)
        {
            GameObject objInstance = null;
            objInstance = Instantiate(info.prefab, info.container.transform);
            objInstance.gameObject.SetActive(false);
            objInstance.transform.position = defaultPosition;
            info.pool.Add(objInstance);
        }
    }

    public GameObject GetPoolObject(PoolObjectType type)
    {
        PoolInfo selected = GetPoolByType(type);
        List<GameObject> pool = selected.pool;

        GameObject objInstance = null;
        if (pool.Count > 0)
        {
            objInstance = pool[pool.Count - 1];
            pool.Remove(objInstance);
        }
        else
            objInstance = Instantiate(selected.prefab, selected.container.transform);

        return objInstance;
    }

    public void CoolObject(GameObject obj, PoolObjectType type)
    {
        obj.SetActive(false);
        obj.transform.position = defaultPosition;

        PoolInfo selected = GetPoolByType(type);
        List<GameObject> pool = selected.pool;

        if (!pool.Contains(obj))
        {
            pool.Add(obj);
        }
    }

    private PoolInfo GetPoolByType(PoolObjectType type)
    {
        for(int i = 0; i < listOfPool.Count; i++)
        {
            if (type == listOfPool[i].type)
                return listOfPool[i];
        }

        return null;
    }
}
