﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

   public enum GameState
   {
      LAUNCHER,
      CONTROLS,
      CREDITS,
      GAMEPLAY,
      PAUSE,
      WIN,
      LOSE,
      GAMEOVERLOSE,
      OPTIONS,
     
   }
