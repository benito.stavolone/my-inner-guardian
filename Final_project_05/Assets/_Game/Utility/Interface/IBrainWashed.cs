﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBrainWashed 
{
    void ChangeTarget();

    void ResetTarget();
}
