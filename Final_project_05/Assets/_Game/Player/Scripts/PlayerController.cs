﻿using AuraAPI;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(GunController))]
[RequireComponent(typeof(AbilityController))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(LineRenderer))]

public class PlayerController : LivingEntity
{
    [Header("Hacker Effect")]
    [SerializeField] private int hackerDamage = 10;
    public int HackerDamage
    {
        get { return hackerDamage; }
    }

    [SerializeField] private Rigidbody rb;
    [SerializeField] private float moveSpeed;
    [SerializeField] private Transform laserOrigin;
    [SerializeField] private LayerMask floorLayer;
    [SerializeField] private GameObject fadeHealth;

    [Header("Torch Ref")]

    [SerializeField] FlashingLiht flashTorch;
    [SerializeField] AuraLight auraRef;

    private Vector3 movements;
    private Vector3 moveVelocity;
    private bool isGrounded = true;
    private float distanceFromFloor = 0.7f;

    private Camera mainCamera;
    private GunController gunController;
    private AbilityController abilityController;
    private LineRenderer lr;
    private int EnemyLayer = 8;

    private GameManager gameManagerRef;

    private bool checkIsGrounded = false;
    public bool CheckIsGrounded
    {
        get { return checkIsGrounded; }
        set { checkIsGrounded = value; }
    }

    protected override void Start()
    {
        base.Start();
        mainCamera = FindObjectOfType<Camera>();
        gunController = GetComponent<GunController>();
        abilityController = GetComponent<AbilityController>();
        lr = GetComponent<LineRenderer>();
        gameManagerRef = GameManager.instance;
        flashTorch.enabled = false;
    }

    private void Update()
    {
        if (gameManagerRef.Isminigamerunning || gameManagerRef.gameState == GameState.PAUSE)
        {      
            return;
        }
        else
        {
            Move();

            if (!gameManagerRef.useController)
                Rotation();
            if (gameManagerRef.useController)
                RotationJoystick();

            Reload();
            Shoot();
        }
    }

    public void FixedUpdate()
    {
        if (gameManagerRef.Isminigamerunning || gameManagerRef.gameState == GameState.PAUSE)
        {
            return;
        }
        else
            rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
    }

    private void Move()
    {
        var DeltaX = Input.GetAxisRaw("Horizontal");
        var DeltaZ = Input.GetAxisRaw("Vertical");
        movements = new Vector3(DeltaX, 0, DeltaZ);

        moveVelocity = movements.normalized * moveSpeed;

        StartCoroutine(Grounded());
    }

    private void Shoot()
    {
        if (!gameManagerRef.useController)
        {
            if (Input.GetMouseButton(0))
            {
                gunController.OnTriggerHold();
            }
            if (Input.GetMouseButtonUp(0))
            {
                gunController.OnTriggerRelease();
            }
            if (Input.GetMouseButtonDown(1))
            {
                abilityController.OnTriggerHold();
            }
        }

        if (gameManagerRef.useController)
        {
            if (Input.GetAxisRaw("JFire1")>0)
                gunController.OnTriggerHold();
            else
                gunController.OnTriggerRelease();

            if (Input.GetAxisRaw("JFire2")>0)
            {
                abilityController.OnTriggerHold();
            }
        }      
    }

    private void Rotation()
    {
        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.up * gunController.GunHeight);
        float rayLenght;
        RaycastHit hit;

        if (groundPlane.Raycast(cameraRay, out rayLenght))
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLenght);

            Vector3 point = new Vector3(pointToLook.x, transform.position.y, pointToLook.z);

            transform.LookAt(point);
            laserOrigin.LookAt(point);

            lr.SetPosition(0, laserOrigin.position);

            if (Physics.Raycast(laserOrigin.position, transform.forward, out hit))
            {
                if (hit.collider)
                {
                    point = hit.point;
                    if (hit.collider.gameObject.layer == EnemyLayer)
                    {
                        lr.material.color = new Color(0, 3, 0);
                    }
                    else
                        lr.material.color = Color.red;
                }
            }

            lr.SetPosition(1, point);


            if ((new Vector3(point.x, point.y, point.z) - new Vector3(transform.position.x,transform.position.y, transform.position.z)).sqrMagnitude > 0.5f)
            {
                gunController.Aim(point);
                abilityController.AimPoint(point);
            }        
        }
    }

    private void RotationJoystick()
    {
        RaycastHit hit;
        Vector3 pointToLook = transform.forward;

        Vector3 playerRotation = Vector3.right * Input.GetAxis("JHorizontal") + 
            Vector3.forward * -Input.GetAxis("JVertical");

        if (playerRotation.sqrMagnitude > 0.0f)
        {
            transform.rotation = Quaternion.LookRotation(playerRotation, Vector3.up);
        }

        Vector3 point = new Vector3(playerRotation.x, transform.position.y, playerRotation.z);

        lr.SetPosition(0, laserOrigin.position);

        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            if (hit.collider)
            {
                pointToLook = hit.point;
                if (hit.collider.gameObject.layer == EnemyLayer)
                {
                    lr.material.color = new Color(0, 3, 0);
                }
                else
                    lr.material.color = Color.red;
            }
        }

        lr.SetPosition(1, pointToLook);

        gunController.Aim(pointToLook);
        abilityController.AimPoint(pointToLook);
    }

    private void Reload()
    {
        if (!gameManagerRef.useController)
        {
            if (Input.GetKeyDown(KeyCode.R))
                gunController.Reload();
        }
        else
        {
            if(Input.GetKey(KeyCode.Joystick1Button2))
                gunController.Reload();
        }

    }

    protected override void OnKill()
    {
        SceneManager.LoadScene(2);
    }

    IEnumerator Grounded()
    {
        float refreshRate = 10f;

        if (checkIsGrounded == false)
        {
            if (!(Physics.Raycast(transform.position, Vector3.down, distanceFromFloor, floorLayer)))
            {
                isGrounded = false;
                rb.constraints = RigidbodyConstraints.None;
                refreshRate = 0.1f;
            }
            else
            {
                if (isGrounded == false)
                {
                    isGrounded = true;
                    rb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
                }
                else
                    refreshRate = 10;
            }
        }
        else
            refreshRate = 1;

        yield return new WaitForSeconds(refreshRate);
    }

    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
        if (damage > 0)
        {
            fadeHealth.SetActive(true);
            Invoke("ResetFadeHealth", 1);
        }
    }

    private void ResetFadeHealth()
    {
        fadeHealth.SetActive(false);
    }

    public void isTimeToFlashTorch(bool value)
    {
        flashTorch.enabled = value;
    }


}
