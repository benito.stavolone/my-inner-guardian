﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameManager : MonoBehaviour
{
    public int numberOfPower;
    public static MinigameManager instance;
    public List<GameObject> minigame;


    private int score;

    private void Awake()
    {
        instance = this;
    }

    private void OnDisable()
    {
        //int typeOfPower = Random.Range(1, numberOfPower + 2);
        //InteractionBtwGames.instance.SetMinigameScore(score);
        //InteractionBtwGames.instance.SetTypeofPower(typeOfPower);
    }

    public void SetScore(int value)
    {
        score = value;
    }
}
