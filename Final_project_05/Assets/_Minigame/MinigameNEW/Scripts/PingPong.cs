﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class PingPong : MonoBehaviour
{
    public Transform a, b;
    [Range(50, 500)]
    public float speed = 1;

    private Transform target;

    private void OnEnable()
    {
        transform.position = a.position;
        target = b;
    }

    void Update()
    {

        if (Vector2.Distance(transform.position, a.position) < 0.5f && target==a)
            target = b;    

        if (Vector2.Distance(transform.position, b.position) < 0.5f && target == b)     
            target = a;
            
        transform.position = Vector2.MoveTowards(transform.position, target.position, Screen.height*0.5f *Time.deltaTime);
    }

    private void OnDisable()
    {
        transform.position = a.position;
    }
}
