﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMove : MonoBehaviour
{
    public float speed;
    public Vector3 startingPosition;
   
    private void Awake()
    {
        startingPosition = transform.position;
    }

    private void Update()
    {
        if(Screen.width <= 800)
        {
            transform.Translate(-new Vector2((Screen.width * 300) / 800, 0) * Time.deltaTime);
        }
        else
        {
            transform.Translate(-new Vector2((Screen.width * 300) / 800, 0) * Time.deltaTime);
        }
    }

    private void OnEnable()
    {
        transform.position = startingPosition;     
    }
}

