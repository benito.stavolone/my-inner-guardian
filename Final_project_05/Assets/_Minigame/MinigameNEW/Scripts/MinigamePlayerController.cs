﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum MiniGame
{
    MINIGAME1,
    MINIGAME2,
    MINIGAME3,
    MINIGAME4,
    MINIGAME5,
    MINIGAME6,
    MINIGAME7,
    MINIGAME8,
    MINIGAME9,
    MINIGAME10,
    MINIGAME11,
    MINIGAME12,
    MINIGAME13,
}
public class MinigamePlayerController : MonoBehaviour
{
    [SerializeField] private MiniGame _minigame;
    public float velocity = 1;
    
    private int currentNumberofObstacles = 0;
    private Rigidbody2D rb;
    Vector3 recoilSmoothDampVelocity;
    private Vector3 originalposition;
    public GameObject spawnPoint;

    public static event Action<MiniGameStatus, MiniGame> OnGameEnd; 

    private void OnEnable()
    {
        currentNumberofObstacles = 0;
        rb = GetComponent<Rigidbody2D>();
        transform.position = spawnPoint.transform.position;
    }
   
    void Update()
    {
        Movement();
    }

    private void Movement()
    {
        if (!GameManager.instance.useController)
        {
            float deltaY = Input.GetAxis("Vertical");
            float deltaZ = deltaY;

            transform.position = new Vector2(spawnPoint.transform.position.x, transform.position.y);

            transform.eulerAngles = new Vector3(0, 0, deltaZ * 20);

            if (Input.GetKey(KeyCode.W))
                transform.Translate(new Vector2(0,Screen.height) * 0.5f * Time.deltaTime);

            if (Input.GetKey(KeyCode.S))
                transform.Translate(-new Vector2(0, Screen.height) * 0.5f * Time.deltaTime);

            if (Input.GetKey(KeyCode.UpArrow))
                transform.Translate(new Vector2(0, Screen.height) * 0.5f * Time.deltaTime);

            if (Input.GetKey(KeyCode.DownArrow))
                transform.Translate(-new Vector2(0, Screen.height) * 0.5f * Time.deltaTime);
        }

        if (GameManager.instance.useController)
        {
            Vector3 playerRotation = 
            Vector3.forward * Input.GetAxis("Vertical");

            transform.position = new Vector2(spawnPoint.transform.position.x, transform.position.y);

            transform.eulerAngles = playerRotation * 20;

            var deltaY = Input.GetAxis("Vertical");

            if(deltaY > 0)
            {
                transform.Translate(new Vector2(0, Screen.height) * 0.5f * Time.deltaTime);
            }
            else if(deltaY < 0)
            {
                transform.Translate(-new Vector2(0, Screen.height) * 0.5f * Time.deltaTime);
            }
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnGameEnd?.Invoke(MiniGameStatus.LOSE, _minigame);       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("finish"))
        {
            OnGameEnd?.Invoke(MiniGameStatus.WIN,_minigame);
        }
    }



}
