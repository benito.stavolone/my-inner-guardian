﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MG_PlayerMovementController : MonoBehaviour
{

    public float velocity = 1;
    public ObstaclesSpawner spawnerRef;
    private int currentNumberofObstacles = 0;
    private Rigidbody2D rb;
    Vector3 recoilSmoothDampVelocity;

    private void OnEnable()
    {
        currentNumberofObstacles = 0;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    private void Movement()
    {
        float deltaY = Input.GetAxis("Vertical");
        float deltaZ = deltaY;

        transform.eulerAngles = new Vector3(0, 0, deltaZ * 25);

        if (Input.GetKey(KeyCode.UpArrow))
            transform.position += Vector3.up * velocity * Time.fixedDeltaTime;

        if (Input.GetKey(KeyCode.DownArrow))
            transform.position += Vector3.down * velocity * Time.fixedDeltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GamePlayGUIManager.instance.DisactiveMiniGamePanel();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        currentNumberofObstacles++;
        if(currentNumberofObstacles == spawnerRef.numberOfObstacles)
            GamePlayGUIManager.instance.DisactiveMiniGamePanel();
    }

    private void OnDisable()
    {
        MinigameManager.instance.SetScore(currentNumberofObstacles);
    }
}
