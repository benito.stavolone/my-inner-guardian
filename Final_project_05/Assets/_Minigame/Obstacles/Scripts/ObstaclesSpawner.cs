﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesSpawner : MonoBehaviour
{

    public float maxTime = 1;
    public int numberOfObstacles = 10;
    public GameObject obstacles;
    private int currentNumberofObstacles;
    public float positionTreshold;

    private float timer;


    private void OnEnable()
    {
        currentNumberofObstacles = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > maxTime)
        {
            if (currentNumberofObstacles < numberOfObstacles)
            {
                GameObject newObstacles = Instantiate(obstacles);
                newObstacles.transform.position = transform.position + new Vector3(0, Random.Range(-positionTreshold, positionTreshold), 0);
                currentNumberofObstacles++;
                newObstacles.transform.SetParent(this.transform);
                timer = 0;
            }
        }
        else
            timer += 0.1f;
    }
}
