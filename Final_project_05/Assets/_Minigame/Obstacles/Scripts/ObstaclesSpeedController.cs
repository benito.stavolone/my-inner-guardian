﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesSpeedController : MonoBehaviour
{
    public float speed;

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.left * speed * Time.fixedDeltaTime;
    }

    private void OnDisable()
    {
        Destroy(gameObject);
    }

}
